var express = require('express');
var path = require('path');
var app = express();

app.use(express.static(path.join(__dirname, 'build')));

// Listen for requests
var server = app.listen(8000, function() {
  console.log('Magic happens on port *:8000');
});
