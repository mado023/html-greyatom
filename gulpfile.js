'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    autoprefix = require('gulp-autoprefixer'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gulpIgnore = require('gulp-ignore'),
    rigger = require('gulp-rigger'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    cleanCSS = require('gulp-clean-css'),
    include  = require("gulp-include");

var path = {
  build: { // production
    html: 'build/',
    css: 'build/assets/styles/',
    adminCss: 'build/setting-dashboard/assets/styles/',
    commitLiveCss: 'build/commit-live/assets/styles/',
    img: 'build/assets/img/',
    adminImg: 'build/setting-dashboard/assets/img/',
    commitLiveImg: 'build/commit-live/assets/img/',
    fonts: 'build/assets/fonts/',   
    bowerfonts: 'build/assets/fonts/',
    fontawesome: 'build/assets/fonts/',
    js: 'build/js/',
  },
  src: { // development
    html: 'src/**/*.html',
    htmlIgonore: '!src/**/vendor/**/*.html',
    style: 'src/assets/styles/main.less',
    adminStyle: 'src/setting-dashboard/assets/styles/main.less',
    commitLiveStyle: 'src/commit-live/assets/styles/main.less',
    mvpStyles: 'src/assets/styles/mvp.less',
    img: 'src/assets/img/**/*.*',
    adminImg: 'src/setting-dashboard/assets/img/**/*.*',
    commitLiveImg: 'src/commit-live/assets/img/**/*.*',
    fonts: 'src/assets/fonts/**/*.*',
    bowerfonts: 'bower_components/bootstrap/fonts/**/*.*',
    fontawesome: 'bower_components/components-font-awesome/fonts/**/*.*',
    js: 'src/js/app.js',
    jsVendor: 'src/js/vendor.js',
  },
  watch: {
    html: 'src/**/*.html',
    style: 'src/assets/styles/**/*.*',
    adminStyle: 'src/setting-dashboard/assets/styles/**/*.*',
    commitLiveStyle: 'src/commit-live/assets/styles/**/*.*',
    img: 'src/assets/img/**/*.*',
    adminImg: 'src/setting-dashboard/assets/img/**/*.*',
    commitLiveImg: 'src/commit-live/assets/img/**/*.*',
    fonts: 'src/assets/fonts/**/*.*',
    bowerfonts: 'bower_components/bootstrap/fonts/**/*.*',
    fontawesome: 'bower_components/components-font-awesome/fonts/**/*.*',
    js: 'src/js/**/*.js',
  },
  clean: './build'
};

// SERVER SETUP
var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend",
    watchTask: true
};

gulp.task('webserver', function () {
    browserSync(config);
});

// COPY HTML
gulp.task('html:build', function () {
    return gulp.src([path.src.html,path.src.htmlIgonore])
        .pipe(include())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

// BUILD JS
gulp.task('jsVendor:build', function () {
    return gulp.src(path.src.jsVendor)
        .pipe(include(
          {
            extensions: "js",
            hardFail: true,
            includePaths: [
              __dirname + "/bower_components",
              // __dirname + "/node_modules",
              __dirname + "/src/js"
            ]
          }
        ))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});
gulp.task('js:build', function () {
    return gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});
// COPY FONTS
gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

// COPY bower FONTS
gulp.task('bowerfonts:build', function() {
    return gulp.src(path.src.bowerfonts)
        .pipe(gulp.dest(path.build.bowerfonts))
});
// COPY FONTAWESOME
gulp.task('fontawesome:build', function() {
    return gulp.src(path.src.fontawesome)
        .pipe(gulp.dest(path.build.fontawesome))
});
//COPY IMAGE
gulp.task('img:build', function (cb) {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img)).on('end', cb).on('error', cb);
});
gulp.task('adminImg:build', function (cb) {
    gulp.src(path.src.adminImg)
        .pipe(gulp.dest(path.build.adminImg)).on('end', cb).on('error', cb);
});
gulp.task('commitLiveImg:build', function (cb) {
    gulp.src(path.src.commitLiveImg)
        .pipe(gulp.dest(path.build.commitLiveImg)).on('end', cb).on('error', cb);
});

// CSS BUILD
gulp.task('style:build:main', function () {
    return gulp.src(path.src.style)
        .pipe(less())
        .pipe(autoprefix({
            browsers: ['last 30 versions', '> 1%', 'ie 8', 'ie 9'],
            cascade: true
        }))
        .pipe(rename('main.source.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});
gulp.task('mvpStyle:build:main', function () {
    return gulp.src(path.src.mvpStyles)
        .pipe(less())
        .pipe(autoprefix({
            browsers: ['last 30 versions', '> 1%', 'ie 8', 'ie 9'],
            cascade: true
        }))
        .pipe(rename('mvp.source.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});
gulp.task('adminStyle:build:main', function () {
    return gulp.src(path.src.adminStyle)
        .pipe(less())
        .pipe(autoprefix({
            browsers: ['last 30 versions', '> 1%', 'ie 8', 'ie 9'],
            cascade: true
        }))
        .pipe(rename('admin.source.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(path.build.adminCss))
        .pipe(reload({stream: true}));
});
gulp.task('commitLiveStyle:build:main', function () {
    return gulp.src(path.src.commitLiveStyle)
        .pipe(less())
        .pipe(autoprefix({
            browsers: ['last 30 versions', '> 1%', 'ie 8', 'ie 9'],
            cascade: true
        }))
        .pipe(rename('commitLive.source.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(path.build.commitLiveCss))
        .pipe(reload({stream: true}));
});

// WATCH

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build:main');
        gulp.start('mvpStyle:build:main');
    });
    watch([path.watch.adminStyle], function(event, cb) {
        gulp.start('adminStyle:build:main');
    });
    watch([path.watch.commitLiveStyle], function(event, cb) {
        gulp.start('commitLiveStyle:build:main');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build:main');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('jsVendor:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.adminImg], function(event, cb) {
        gulp.start('adminImg:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.bowerfonts], function(event, cb) {
        gulp.start('bowerfonts:build');
    });
    watch([path.watch.fontawesome], function(event, cb) {
        gulp.start('fontawesome:build');
    });
});


//CLEAN PRODUCTION
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// BUILD TASK
gulp.task('build', [
  'html:build',
  'jsVendor:build',
  'js:build',
  'fonts:build',  
  'bowerfonts:build',
  'fontawesome:build',
  'img:build',
  'adminImg:build',
  'commitLiveImg:build',
  'style:build:main',
  'mvpStyle:build:main',
  'adminStyle:build:main',
  'commitLiveStyle:build:main',
]);

// DEFAULT TASK
gulp.task('default', ['build', 'webserver','watch']);
